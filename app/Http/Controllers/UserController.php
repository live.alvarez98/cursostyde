<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Models\User;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    public function index(){


        //$users = DB::table('users')->get(); 
        $users = User::all();
        

        $title = 'Listado de usuarios';

        
        //return view('users',['users'=>$users,'title'=> $title]);
        //return view('users')->with('users',$users)->with('title','Listado de usuarios');
        return view('users.index',compact('users','title'));

    }

    public function show(User $user){
        //$user = User::findOrFail($id);

        return view('users.show',compact('user'));
        //return "Mostrando detalle del usuario {$id}";
        
    }

    public function create(){
        return view('users.create');
    }

    public function store(Request $request){

        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => ['required', Password::min(6)],
        
        ],[
            'name.required'=>'EL campo "Nombre" es obligatorio',
            'email.required'=>'EL campo "Correo electronico" es obligatorio',
            'password.required'=>'EL campo "Contraseña" es obligatorio',
            'password.min'=> 'La contraseña requiere 6 caracteres como mínimo',
            'email.email'=>'EL correo electronico NO está escrito en el formato correcto',
            'email.unique'=>'EL correo electronico ya está registrado',
        ]);

        
        /* if(empty($data['name'])){
            return redirect()->route('users.create')->withErrors([
                'name'=>'Campo obligatorio'
            ]);
        } */
        
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return redirect()->route('users.index');
    }


    public function edit(User $user){
        return view('users.edit',compact('user'));
    }

    public function update(User $user){

        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => '',
        
        ],[
            'name.required'=>'EL campo "Nombre" es obligatorio',
            'email.required'=>'EL campo "Correo electronico" es obligatorio',
            'email.email'=>'EL correo electronico NO está escrito en el formato correcto',
            'email.unique'=>'EL correo electronico ya está registrado',
        ]);
        
        if($data['password']!=NULL){

            $data += request()->validate([
                    'password' => [ Password::min(6)], 
                ],[  
                    'password.min'=> 'La contraseña requiere 6 caracteres como mínimo',     
            ]);

            $data['password'] = bcrypt($data['password']);

        }else{
            unset($data['password']);
        }

        
        $user->update($data);
        
        
        return redirect("/usuarios/{$user->id}");

    }

    public function destroy(User $user){
     
        $user->delete();
        
        return redirect("/usuarios");

    }
}
