<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\User;
use \App\Models\Profession;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //$professions = DB::select('SELECT id FROM professions where title = ? LIMIT 0,1',['Desarrollador back-end']);

       $professionId = Profession::where(['title' => 'Desarrollador back-end'])->value('id');

        User::factory()->make([
            'name'=>'Jesus',
            'email' =>'jesus@email.com',
            'password' => bcrypt('contraseña'),
            'profession_id' => $professionId,
            'is_admin' => true,

        ])->save();

       

        User::factory()->make([
            'profession_id'=> $professionId,
        ])->save();

        User::factory()->count(10)->create();
        
        
    }
}
