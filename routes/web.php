<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/usuarios', [UserController::class, 'index'])->name('users.index');
Route::get('/usuarios/nuevo', [UserController::class, 'create'])->name('users.create');
Route::get('/usuarios/{user}', [UserController::class, 'show'])->where('user','[0-9]+')->name('users.show');
Route::get('/usuarios/{user}/editar', [UserController::class, 'edit'])->where('user','[0-9]+')->name('users.edit');


Route::post('/usuarios/crear',[UserController::class,'store'])->name('users.store');
Route::put('/usuarios/{user}',[UserController::class,'update'])->name('users.update');

Route::delete('/usuarios/{user}',[UserController::class,'destroy'])->name('users.delete');



Route::get('/saludo/{name}/{nickname?}', [WelcomeUserController::class,'index']);
