@extends('layout')

@section('title','Crear usuario')

@section('content')
        
            
        
        <div class="card">
            <h2 class="card-header">Crear nuevo usuario</h2>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger" >
                    <h4>Por favor corrige los siguientes errores en los campos:</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                  
                </div>
        
                @endif
                <div class="container center-h">
                    <form method="POST" action="{{route('users.store')}}" style="display:  width:50%;">
                        @csrf
                        <label for="name">Nombre</label>
                        <br>
                        <input type="text" name="name" id="name" value="{{old('name')}}">
        
                        <br><br>
        
                        <label for="email">Correo electronico</label>
                        <br>
                        <input type="email" name="email" id="email" value="{{old('email')}}">
        
                        <br><br>
        
                        <label for="password">Contraseña</label>
                        <br>
                        <input type="password" name="password" id="password" aria-describedby="passwordHelpInline">
                        <br>
                        <span id="passwordHelpInline" class="form-text">
                            Debe tener al menos 6 caracteres.
                        </span>
             
                        <br><br>
                    
                        <button class="btn btn-primary"  type="submit">Crear usuario</button>
                        <a class="btn btn-link" href="{{route('users.index')}}">Regresar</a>
                        
                    
                            
                    </form>
                </div>
        
      
            </div>
          </div>

        
        
        
      
@endsection
