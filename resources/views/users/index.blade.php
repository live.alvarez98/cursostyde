@extends('layout')
@section('title',"Usuarios")
@section('content')

    
    <div class="row align-items-end mb-2">
          <div class="col-12 col-sm-10">
            <h1 >{{ ($title) }}</h1>
           </div>
          <div class="col-12 col-sm-2">
              <a href="{{route('users.create')}}"   class="btn btn-primary">Agregar usuario</a>
          </div>
      </div>
    <table class="table table-dark table-striped">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Email</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($users as $user)
            <tr>
              <th scope="row">{{ $user->id }}</th>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email}}</td>
              <td>
                <a href="{{route('users.show',['user'=>$user])}}" class="btn btn-link"><span class="oi oi-eye"></span></a>
                <a href="{{route('users.edit',['user'=>$user])}}" class="btn btn-link"><span class="oi oi-pencil"></span></a>
              </td>
              
            </tr>
            @empty
                <p>No hay usuarios registrados</p>
            @endforelse
           
          </tbody>
    </table>
       

        
        

    
@endsection


