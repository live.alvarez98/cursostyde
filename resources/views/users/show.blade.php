@extends('layout')

@section('title',"Usuario {$user->name}")

@section('content')
           

            <div class="card">
                
                <div class="card-header">
                    Usuario #{{$user->id}}
                </div>
                <div class="card-body">
                <h5 class="card-title">{{$user->name}}</h5>
                <p class="card-text">Email: {{$user->email}}</p>
                @if ($user->profession)
                    <p class="card-text">Profesión: {{$user->profession->title}}</p>
                @else
                    <p class="card-text">Profesión: Ninguna</p>
                @endif
                <div class="row">
                    <div class="col col-sm-1">
                        <a href="{{route('users.edit',$user)}}" class="btn btn-primary">Editar</a>
                    </div>
                    <div class="col">
                        <form METHOD="POST" action="{{route('users.delete',$user)}}">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger" type="submit"">Borrar</button>
                        
                        </form>
                    </div>
                 </div>
             

                </div>
                
            </div>
            <a href="{{route('users.index')}}">Regresar</a>
            
        
        
      
@endsection
