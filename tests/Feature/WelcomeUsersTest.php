<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WelcomeUsersTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    public function it_welcomes_users_with_nickname()
    {
        $this->get('/saludo/jesus/apodo')
            ->assertStatus(200)
            ->assertSee('Bienvenido jesus, tu apodo es apodo');
    }
     /**
     * @test
     */

    public function it_welcomes_users_without_nickname()
    {
        $this->get('/saludo/jesus')
            ->assertStatus(200)
            ->assertSee('Bienvenido jesus');
    }
}
