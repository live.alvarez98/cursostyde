<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Profession;
use Illuminate\Support\Facades\DB;


class UsersModuleTest extends TestCase
{
   
    use RefreshDatabase;
     /**
     * A basic feature test example.
     *
     * @test
     */
    public function it_loads_the_users_list_page()
    {

        User::factory()->make([
            'name'=>'Jesus',
            'password' => bcrypt('contraseña'),
            'is_admin' => true,
    
        ])->save();
    
       
    
        User::factory()->count(10)->create();
        $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee('Listado de usuarios')
            ->assertSee('Jesus');
       
    }
      /**
     *
     * @test
     */
    public function it_shows_a_message_if_there_are_no_users()
    {
       
        $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee('No hay usuarios registrados');
          
    }
     /**
     * @test
     */

    public function it_displays_the_users_details()
    {

        $user = User::factory()->create([
            'name'=>'Jesus Carmona',
            'password' => bcrypt('contraseña'),
            'is_admin' => true,
    
        ]);


        $this->get('/usuarios/'.$user->id)
            ->assertStatus(200)
            ->assertSee($user->name)
            ->assertSee($user->email);
    }
      /**
     * @test
     */

    public function it_displays_a_404_error()
    {

       $this->get('/usuarios/999')
            ->assertStatus(404)
            ->assertSee('Pagina no encontrada');
    }

     /**
     * @test
     */

    public function it_welcomes_new_users_page()
    {
        $this->get('/usuarios/nuevo')
            ->assertStatus(200)
            ->assertSee('Crear nuevo usuario');
    }

    
     /**
     * @test
     */
    public function it_create_a_new_user(){

        $this->post('/usuarios/crear', [
            'name' => 'Jesus',
            'email' => 'email@email.comn',
            'password' => '123456',
            
        ])->assertRedirect('/usuarios');

        //$this->asserDatabaseHas('users',[
        $this->assertCredentials([
            'name' => 'Jesus',
            'email' => 'email@email.comn',
            'password' => '123456',
        ]);

        
    }
    
     /**
     * @test
     */

    public function the_name_is_required(){

        
        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'email' => 'email@email2.comn',
            'password' => '123',
            

        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['name' => 'EL campo "Nombre" es obligatorio']); 

        $this->assertDatabaseMissing('users',[
            'email' => 'email@email2.comn',
            'password' => '123',
        ]);
    }

    /**
     * @test
     */

    public function the_email_is_required(){

        
        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'name' => 'Jesus Angel',
            'password' => '1234',
            

        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['email' => 'EL campo "Correo electronico" es obligatorio']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel',
            'password' => '1234',
        ]);
    }

    /**
     * @test
     */
    public function the_password_is_required(){

        
        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'name' => 'Jesus Angel2',
            'email' => 'email2@email2.comn',
    

        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['password' => 'EL campo "Contraseña" es obligatorio']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel2',
            'email' => 'email2@email2.comn',
        ]);
    }

     /**
     * @test
     */

    public function the_email_is_not_valid(){

        
        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'name' => 'Jesus Angel',
            'email' => 'correo-no-valido',
            'password' => '1234',
            

        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['email' => 'EL correo electronico NO está escrito en el formato correcto']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel',
            'email' => 'correo-no-valido',
            'password' => '1234',
        ]);
    }

     /**
     * @test
     */

    public function the_email_is_unique(){

        
        User::create([
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus@email.com',
            'password' => '1234',
        ]);

        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus@email.com',
            'password' => '1234',
            
        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['email' => 'EL correo electronico ya está registrado']); 

       $this->assertEquals(1, User::count());
    }

     /**
     * @test
     */

    public function the_password_does_not_have_the_minimum_characters(){


        $this->from('usuarios/nuevo')->post('/usuarios/crear', [
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus@email.com',
            'password' => '12345',
            
        ])->assertRedirect('usuarios/nuevo')
        ->assertSessionHasErrors(['password' => 'La contraseña requiere 6 caracteres como mínimo']); 

       $this->assertEquals(0, User::count());
    }

    /**
     * @test
     */

    public function itloads_edit_users_page()
    {
        $user = User::factory()->create();

        $this->get("/usuarios/{$user->id}/editar")
            ->assertStatus(200)
            ->assertViewIs('users.edit')
            ->assertSee('Editar usuario')
            ->assertViewHas('user',function($viewUser) use($user){
                return $viewUser->id === $user->id;
            });
           
    }

    /**
     * @test
     */
    public function it_updates_a_user(){

        $user = User::factory()->create();

        $this->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel',
            'email' => 'email1@email.comn',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}");
        
        $this->assertCredentials([
            'name' => 'Jesus Angel',
            'email' => 'email1@email.comn',
            'password' => '123456',
        ]);

        
    }

     /**
     * @test
     */

    public function the_name_is_required_when_updating_user(){

        $user = User::factory()->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => '',
            'email' => 'email21@email.comn',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['name']);

        $this->assertDatabaseMissing('users',[
            'email' => 'email21@email.comn',
            'password' => '123456',

        ]);
    }

        /**
     * @test
     */

    public function the_email_is_required_when_updating_user(){

        
        $user = User::factory()->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel',
            'email' => '',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel',
            'password' => '123456',

        ]);
    }

    /**
     * @test
     */
    public function the_password_is_optional_when_updating_user(){

        
        $user = User::factory()->create([
            'password' => bcrypt('clave_anterior')
        ]);

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel',
            'email' => 'email@emailtest.com',
            'password' => '',
            
        ])->assertRedirect("/usuarios/{$user->id}");
        

        $this->assertCredentials([
            'name' => 'Jesus Angel',
            'email' => 'email@emailtest.com',
            'password' => 'clave_anterior'

        ]);
    }

     /**
     * @test
     */

    public function the_email_is_not_valid_when_updating_user(){

    
        $user = User::factory()->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel',
            'email' => 'correo-no-valido',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['email' => 'EL correo electronico NO está escrito en el formato correcto']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel',
            'email' => 'correo-no-valido',
            'password' => '123456',

        ]);
    }

     /**
     * @test
     */

    public function the_email_is_unique_when_updating_user(){


        
        User::create([
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus@email.com',
            'password' => '123456',
        ]);

        $user = User::factory()->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel Carmona2',
            'email' => 'correojesus@email.com',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['email' => 'EL correo electronico ya está registrado']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel Carmona2',
            'email' => 'correojesus@email.com',
            'password' => '123456',

        ]);
    } 

    /**
     * @test
     */

     public function the_email_can_stay_the_same_when_updating_user(){

        
        $user = User::create([
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus@email.com',
            'password' => '123456',
        ]);


        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel Carmona2',
            'email' => 'correojesus@email.com',
            'password' => '123456',
            
        ])->assertRedirect("/usuarios/{$user->id}"); 

        $this->assertDatabaseHas('users',[
            'name' => 'Jesus Angel Carmona2',
            'email' => 'correojesus@email.com',

        ]);
        
    } 

     /**
     * @test
     */

    public function the_password_does_not_have_the_minimum_characters_when_updating_user(){
        $user = User::factory()->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus2@email.com',
            'password' => '12345',
            
        ])->assertRedirect("/usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['password' => 'La contraseña requiere 6 caracteres como mínimo']); 

        $this->assertDatabaseMissing('users',[
            'name' => 'Jesus Angel Carmona',
            'email' => 'correojesus2@email.com',
            'password' => '12345',

        ]);

    }

    /**
     * @test
     */
    public function it_deletes_a_user(){
        $user = User::factory()->create([
            'name' => 'Jesus Alvarez',
            'email' => 'prueba@email.com'
        ]);

        $this->delete("usuarios/{$user->id}")
        ->assertRedirect('usuarios');

        $this->assertDatabaseMissing('users',[
            'id' => $user->id,
            'name' => 'Jesus Alvarez',
            'email' => 'prueba@email.com',
        ]);

    }
}
